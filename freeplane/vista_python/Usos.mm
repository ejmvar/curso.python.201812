<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Ejemplos" FOLDED="false" ID="ID_191153586" CREATED="1551276150495" MODIFIED="1552396963530" ICON_SIZE="36.0 pt" STYLE="oval">
<font SIZE="22"/>
<hook NAME="MapStyle" zoom="1.001">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="29" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<hook URI="python-logo-generic-11-2.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Cient&#xed;fico" POSITION="left" ID="ID_1747888601" CREATED="1551452475390" MODIFIED="1551452484253">
<edge COLOR="#00ff00"/>
<node TEXT="General" ID="ID_1756339349" CREATED="1551452489474" MODIFIED="1551452493192">
<node TEXT="Pandas" ID="ID_1185090958" CREATED="1551452721215" MODIFIED="1551452734139"/>
<node TEXT="MatPlotLib" ID="ID_58713326" CREATED="1551276150498" MODIFIED="1552399518324" HGAP_QUANTITY="101.80488213352491 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<font BOLD="true"/>
<node TEXT="L&#xed;nea p/valores" ID="ID_648006414" CREATED="1551276150498" MODIFIED="1551283151832" LINK="http://localhost:8888/notebooks/matplotlib/Matplotlib_line.ipynb"/>
<node TEXT="Varias distribuciones" ID="ID_419715123" CREATED="1551276150498" MODIFIED="1551283278885" LINK="http://localhost:8888/notebooks/matplotlib/sample_plots.ipynb" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-14.926829962699236 pt"/>
<node ID="ID_943609117" CREATED="1551283313987" MODIFIED="1551283322822" LINK="http://localhost:8888/notebooks/matplotlib/NumPy%20Matrices.ipynb"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#000000">Matrices</font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="NumPy" ID="ID_1643052921" CREATED="1551452712942" MODIFIED="1551452729170"/>
<node TEXT="SymPy" ID="ID_872969240" CREATED="1551452624624" MODIFIED="1551452628240"/>
<node TEXT="Scikit" ID="ID_1750452017" CREATED="1551452760071" MODIFIED="1551452765398"/>
</node>
<node TEXT="Matem&#xe1;ticas simb&#xf3;licas" ID="ID_1894809761" CREATED="1551452529932" MODIFIED="1551452623345"/>
<node TEXT="&#xc1;lgebra matricial" ID="ID_1027612911" CREATED="1551452782674" MODIFIED="1551452790442"/>
<node TEXT="Estad&#xed;sticas" ID="ID_1870824964" CREATED="1551452837386" MODIFIED="1551453019367"/>
<node TEXT="Machine Learning" ID="ID_445508509" CREATED="1551453019697" MODIFIED="1551453030398"/>
<node TEXT="Gen&#xe9;tica" ID="ID_533413847" CREATED="1552396982614" MODIFIED="1552396989498"/>
<node TEXT="Estudios celulares&#xa;y por im&#xe1;genes" ID="ID_469452621" CREATED="1552396990277" MODIFIED="1552397018327"/>
<node TEXT="Ingenier&#xed;as: varias" ID="ID_380904605" CREATED="1552397027839" MODIFIED="1553809333490"/>
</node>
<node POSITION="left" ID="ID_575079876" CREATED="1551276150498" MODIFIED="1552399625022" HGAP_QUANTITY="67.56097810145019 pt" VSHIFT_QUANTITY="-42.14634342409195 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>WLST</b>&#160;(<b>Jython</b>)
    </p>
    <p>
      Weblogic Scripting Tool
    </p>
  </body>
</html>
</richcontent>
<edge COLOR="#0000ff"/>
<node TEXT="Ejemplo OAS11g" ID="ID_317662625" CREATED="1551276150498" MODIFIED="1551277450969" LINK="http://sqltech.cl/doc/oas11gR1/web.1111/e13715/using_wlst.htm" HGAP_QUANTITY="14.878048821335248 pt" VSHIFT_QUANTITY="-18.439025248040235 pt"/>
<node TEXT="Script" ID="ID_1522387997" CREATED="1551276150498" MODIFIED="1551277610352" LINK="http://www.munzandmore.com/2013/ora/python-jython-wlst-weblogic"/>
</node>
<node TEXT="Aplicaciones varias..." POSITION="left" ID="ID_743709150" CREATED="1552595784417" MODIFIED="1552595793979">
<edge COLOR="#00007c"/>
<node ID="ID_1842196671" CREATED="1552595796440" MODIFIED="1552595796440" LINK="https://calibre-ebook.com/"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul http-equiv="content-type" content="text/html; charset=utf-8" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0; font-size: 1.125rem; color: rgb(51, 51, 51); font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <li style="margin-top: 0px; margin-right: 0px; margin-bottom: 0; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px">
        <a title="Calibre ebook" href="https://calibre-ebook.com/" target="_blank" rel="noopener nofollow" style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; color: rgb(2, 117, 216); text-decoration: none"><font color="rgb(2, 117, 216)">Calibre</font></a>: el mejor gestor de e-books para todos los usuarios.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="BitTorrent: programa para compartir ficheros de tipo torrent est&#xe1;ndar" ID="ID_1965490618" CREATED="1552595806939" MODIFIED="1552595816528"/>
<node TEXT="Odoo (antes OpenERP): un ERP y mucho m&#xe1;s para la gesti&#xf3;n de empresas, de software libre." ID="ID_682655190" CREATED="1552595817850" MODIFIED="1552595819340"/>
<node TEXT="Flask: web microframework for Python" ID="ID_916208611" CREATED="1552595820394" MODIFIED="1552595862996"/>
<node TEXT="Django, Web2Py: otros frameworks" ID="ID_1947595629" CREATED="1552595883173" MODIFIED="1552595954989"/>
</node>
</node>
</map>
