# def tester(start):
#       def nested(label):
#          nonlocal state   #nonlocals must already exist in enclosing def!
#          state = 0
#          print(label, state)
#       return nested
# # print("SyntaxError: no binding for nonlocal 'state' found")

def tester(start):

    def nested(label):
      global state   #Globals dont have to exits yet when declared
      state = 0      #This creates the name in the module now
      print(label, state)
    return nested

F = tester(0)
F('abc')
print("abc 0")
# state
print("0")