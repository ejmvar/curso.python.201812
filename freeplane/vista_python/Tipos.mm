<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Tipos" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1551454199648"><hook NAME="MapStyle" zoom="1.1">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<hook NAME="AutomaticEdgeColor" COUNTER="15" RULE="ON_BRANCH_CREATION"/>
<node TEXT="data" POSITION="right" ID="ID_496549253" CREATED="1545067808207" MODIFIED="1551293721039" LINK="http://localhost:8888/notebooks/00-02-Tipos-Introspeccion.ipynb">
<edge COLOR="#ff0000"/>
<node TEXT="built-in" ID="ID_554950492" CREATED="1545067826039" MODIFIED="1551285222747" LINK="https://docs.python.org/3/library/stdtypes.html">
<node TEXT="Nulo: None" ID="ID_236826632" CREATED="1549306329224" MODIFIED="1549306337517"/>
<node TEXT="L&#xf3;gico: bool (Truethy/Falsy)" ID="ID_1838932999" CREATED="1549305912611" MODIFIED="1551286023378"/>
<node TEXT="Enteros : int (Py2)" ID="ID_290540534" CREATED="1549305774553" MODIFIED="1551284082179"/>
<node TEXT="Enteros largos: &gt;20d&#xed;gitos: (Py2: 2L es long , Py3: todos)" ID="ID_1941197841" CREATED="1549305782426" MODIFIED="1551285244671" LINK="http://localhost:8888/notebooks/00-00-Tipos-base.ipynb"/>
<node TEXT="float: 1f" ID="ID_1196603070" CREATED="1549305791737" MODIFIED="1549306378275"/>
<node TEXT="complex" ID="ID_1719129199" CREATED="1545320302868" MODIFIED="1545320313100">
<node TEXT="imaginario: 1j" ID="ID_655698980" CREATED="1549305798863" MODIFIED="1549306367705"/>
<node TEXT="3+4j" ID="ID_819854755" CREATED="1549306552908" MODIFIED="1549306562100"/>
</node>
</node>
<node TEXT="complex" ID="ID_1753012442" CREATED="1549310058238" MODIFIED="1549310062855">
<node TEXT="tuples (a, ...)" ID="ID_933566290" CREATED="1549310063812" MODIFIED="1549310098255"/>
<node TEXT="arrays [b, ...]" ID="ID_1406082457" CREATED="1549310099786" MODIFIED="1549310377804"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="3">Mutable </font>
    </p>
    <p>
      <font size="3">Slice: </font>
    </p>
    <p>
      <font size="3">* [ini:fin] : de 0 hasta el anterior a <b>fin</b>&#160; </font>
    </p>
    <p>
      <font size="3">* [ini:], [:fin] : hasta el fin/desde el pcpio </font>
    </p>
    <p>
      <font size="3">* [:] = copia</font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="sets{a,2,True,...}" ID="ID_1854442905" CREATED="1549310127763" MODIFIED="1549310386827"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="3">elementos distintos </font>
    </p>
    <p>
      <font size="3">elems inmutables</font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="dicts {1:&quot;s&quot;, &quot;f&quot;:21.2, ...}" ID="ID_1898903889" CREATED="1549310150340" MODIFIED="1551284633004"/>
</node>
<node TEXT="stdlib" ID="ID_1141083166" CREATED="1551288867177" MODIFIED="1551289645241" LINK="http://localhost:8888/notebooks/00-01-Tipos-base-StdLib.ipynb">
<node TEXT="decimal" ID="ID_1093155142" CREATED="1551288876546" MODIFIED="1551288881481"/>
<node TEXT="fraction" ID="ID_623731389" CREATED="1551288881967" MODIFIED="1551288884936"/>
</node>
<node TEXT="operations" ID="ID_204712603" CREATED="1549309932117" MODIFIED="1551289688632" LINK="https://docs.python.org/3/library/stdtypes.html">
<node TEXT="boolean" ID="ID_1661131454" CREATED="1551285879294" MODIFIED="1551285882205"/>
<node TEXT="numeric" ID="ID_1696491267" CREATED="1549309942789" MODIFIED="1549309947272">
<node TEXT="2.x" ID="ID_1459811678" CREATED="1549309994574" MODIFIED="1549310009810" LINK="https://docs.python.org/2.4/lib/typesnumeric.html"/>
</node>
<node TEXT="bitwise" ID="ID_1956584152" CREATED="1549309951171" MODIFIED="1549309957073">
<node TEXT="2.x" ID="ID_1441673606" CREATED="1549310026560" MODIFIED="1549310034456" LINK="https://docs.python.org/2.4/lib/bitstring-ops.html"/>
</node>
<node TEXT="iterators" ID="ID_83411964" CREATED="1549310951273" MODIFIED="1551293865853">
<node TEXT="2.x" ID="ID_1194009214" CREATED="1551293846276" MODIFIED="1551293855561" LINK="http://localhost:8888/notebooks/00-REPL/itertools_py2.ipynb"/>
<node TEXT="2.x itertools" ID="ID_692095663" CREATED="1551296205072" MODIFIED="1551296218080" LINK="https://medium.com/@jasonrigden/a-guide-to-python-itertools-82e5a306cdf8"/>
</node>
</node>
<node TEXT="objects" ID="ID_1847811984" CREATED="1545320263937" MODIFIED="1551293758142" LINK="http://localhost:8888/notebooks/00-02-Tipos-Introspeccion.ipynb">
<node TEXT="functions (as objects)" ID="ID_935643675" CREATED="1545320314404" MODIFIED="1551287775009"/>
<node TEXT="classes" ID="ID_1501403438" CREATED="1545320326974" MODIFIED="1545320329484"/>
<node TEXT="methods" ID="ID_1144884096" CREATED="1545247876821" MODIFIED="1545247881706"/>
<node TEXT="attributes" ID="ID_411862346" CREATED="1545247882594" MODIFIED="1545247886025"/>
</node>
<node TEXT="introspection" ID="ID_928603386" CREATED="1545067829306" MODIFIED="1549307554961">
<node TEXT="dir()" ID="ID_333472996" CREATED="1549307393344" MODIFIED="1549310399065"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="3">Attributes/methods </font>
    </p>
    <p>
      <font size="3">Object/local scope</font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="__doc__" ID="ID_1245132138" CREATED="1549307467424" MODIFIED="1549310427036"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="3">Documentaci&#243;n del objeto, clase, m&#243;dulo, package</font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="methods/functions" POSITION="right" ID="ID_1745219740" CREATED="1546633276153" MODIFIED="1546633405631">
<edge COLOR="#808080"/>
<node TEXT="builtin" ID="ID_1206564894" CREATED="1545324160169" MODIFIED="1546633250916" HGAP_QUANTITY="108.49999718368062 pt" VSHIFT_QUANTITY="0.7499999776482589 pt">
<node TEXT="len()" ID="ID_89575058" CREATED="1547497712551" MODIFIED="1547497716516"/>
<node TEXT="dir()" ID="ID_1126434509" CREATED="1547497692844" MODIFIED="1547497696794"/>
<node TEXT="next()" ID="ID_681581131" CREATED="1545324186063" MODIFIED="1545324189885"/>
</node>
<node TEXT="user defined" ID="ID_914991479" CREATED="1546633383940" MODIFIED="1546633396615"/>
</node>
<node TEXT="classes" POSITION="right" ID="ID_368676462" CREATED="1545325890138" MODIFIED="1545325915785">
<edge COLOR="#808080"/>
<node TEXT="definition" ID="ID_784197343" CREATED="1545325917665" MODIFIED="1545325927876"/>
<node TEXT="inheritance" ID="ID_1078492080" CREATED="1545325932891" MODIFIED="1547497858029">
<node TEXT="multiple" ID="ID_412819361" CREATED="1547497996609" MODIFIED="1547498002404"/>
</node>
<node TEXT="polimorphism" ID="ID_1550877894" CREATED="1547497863511" MODIFIED="1547497869704"/>
<node TEXT="delegation" ID="ID_128324461" CREATED="1547497872773" MODIFIED="1547497876146"/>
</node>
</node>
</map>
