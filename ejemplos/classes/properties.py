from pprint import pprint

LISTA_DE_NOMBRES = (
    'Juan', 'Pedro', 'Luis',
    'Malena', 'Canta', 'el Tango',
)


class NombresBase:
    pass


# ----- ----- ----- ----- ----- ----- -----
nombre_base = NombresBase()
nombre_base.name = 'Pirulo'

pprint('nombre_base: %s' % nombre_base.name)
print("-" * 30)


# ----- ----- ----- ----- ----- ----- -----

class NombreInit:
    def __init__(self, name):
        self.name = name


nombre_init = NombreInit('Pirulo')

pprint('nombre_init: %s' % nombre_init.name)
print("-" * 30)


# ----- ----- ----- ----- ----- ----- -----

class NombrePriv:
    def __init__(self, name):
        self._name = name


nombre_priv = NombrePriv('Pirulo')

pprint('nombre_priv.name: %s' % getattr(nombre_priv, 'name', "No existe!"))
pprint('nombre_priv._name: %s' % nombre_priv._name)
print("-" * 30)


# ----- ----- ----- ----- ----- ----- -----

class NombreRdOnly:
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name


nombre_rdonly = NombreRdOnly('Pirulo')

pprint('nombre_rdonly.name: %s' % getattr(nombre_rdonly, 'name', "No existe!"))
pprint('nombre_rdonly._name: %s' % nombre_rdonly._name)
try:
    nombre_rdonly.name = "Dos Pirulos"
except AttributeError as exc:
    print("EXC AttributeError: %s" % exc)
pprint('nombre_rdonly.name: %s' % getattr(nombre_rdonly, 'name', "No existe!"))
pprint('nombre_rdonly._name: %s' % nombre_rdonly._name)
print("-" * 30)


# ----- ----- ----- ----- ----- ----- -----

class NombreRdWr:
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name + '+Out:ok'

    @name.setter
    def name(self, name):
        if name not in LISTA_DE_NOMBRES:
            raise ValueError('%s no figura en la LISTA_DE_NOMBRES' % name)
        self._name = name + '+In:ok'


nombre_rdwr = NombreRdWr('Pirulo')

pprint('nombre_rdwr.name: %s' % getattr(nombre_rdwr, 'name', "No existe!"))
pprint('nombre_rdwr._name: %s' % nombre_rdwr._name)
try:
    nombre_rdwr.name = "Dos Pirulos"
except AttributeError as exc:
    print("EXC AttributeError: %s" % exc)
except ValueError as exc:
    print("EXC ValueError: %s" % exc)

pprint('nombre_rdwr.name: %s' % getattr(nombre_rdwr, 'name', "No existe!"))
pprint('nombre_rdwr._name: %s' % nombre_rdwr._name)
print("-" * 30)

nombre_rdwr = NombreRdWr('Canta')

pprint('nombre_rdwr.name: %s' % getattr(nombre_rdwr, 'name', "No existe!"))
pprint('nombre_rdwr._name: %s' % nombre_rdwr._name)
print("-" * 20)
try:
    nombre_rdwr.name = "el Tango"
except AttributeError as exc:
    print("EXC AttributeError: %s" % exc)
except ValueError as exc:
    print("EXC ValueError: %s" % exc)

pprint('nombre_rdwr.name: %s' % getattr(nombre_rdwr, 'name', "No existe!"))
pprint('nombre_rdwr._name: %s' % nombre_rdwr._name)
print("-" * 20)

try:
    nombre_rdwr._name = "Pedro"
except AttributeError as exc:
    print("EXC AttributeError: %s" % exc)
except ValueError as exc:
    print("EXC ValueError: %s" % exc)

pprint('nombre_rdwr.name: %s' % getattr(nombre_rdwr, 'name', "No existe!"))
pprint('nombre_rdwr._name: %s' % nombre_rdwr._name)
print("-" * 30)

try:
    nombre_rdwr._name = "Pedro PicaPiedras"
except AttributeError as exc:
    print("EXC AttributeError: %s" % exc)
except ValueError as exc:
    print("EXC ValueError: %s" % exc)

pprint('nombre_rdwr.name: %s' % getattr(nombre_rdwr, 'name', "No existe!"))
pprint('nombre_rdwr._name: %s' % nombre_rdwr._name)
print("-" * 30)

# ----- ----- ----- ----- ----- ----- -----
import random


class NombreStatic:
    novedades = ('No', 'le', 'cante')

    def __init__(self, name):
        self._name = name

    @staticmethod
    def ver_en_lista(name):
        return name in LISTA_DE_NOMBRES

    @staticmethod
    def gen_from_lista():
        return random.choice(LISTA_DE_NOMBRES)

    @classmethod
    def gen_from_clase(cls, nm):
        return "%s : %s" % (nm, nm in cls.novedades)

    @classmethod
    def ver_por_clase(cls, name):
        return name in LISTA_DE_NOMBRES

    @classmethod
    def NombresValidos(cls):
        return cls.novedades


nombre_static = NombreStatic('Ciruelazo')
for n in ("xxxx", 'Canta'):
    print("*NombreStatic: pertenece '%s'? %s" % (n, NombreStatic.ver_en_lista(n)))
    print("nombre_static: pertenece '%s'? %s" % (n, nombre_static.ver_en_lista(n)))

    print("*NombreStatic: ver_por_clase '%s'? %s" % (n, NombreStatic.ver_por_clase(n)))
    print("nombre_static: ver_por_clase '%s'? %s" % (n, nombre_static.ver_por_clase(n)))

print("*NombreStatic: pertenece '%s'? %s" % (nombre_static._name, NombreStatic.ver_en_lista(n)))
print("nombre_static: pertenece '%s'? %s" % (nombre_static._name, nombre_static.ver_en_lista(n)))

print("*NombreStatic: ver_por_clase '%s'? %s" % (nombre_static._name, NombreStatic.ver_por_clase(n)))
print("nombre_static: ver_por_clase '%s'? %s" % (nombre_static._name, nombre_static.ver_por_clase(n)))

print("*NombreStatic.gen_from_lista: '%s'" % (NombreStatic.gen_from_lista()))
print("nombre_static.gen_from_lista: '%s'" % (nombre_static.gen_from_lista()))

# en otro módulo, debería importar la lista (constante)
# una alternativa es tomar la definición desde DENTRO DE LA LISTA

print('* NombreStatic.NombresValidos')
print(NombreStatic.NombresValidos())

print("Novedades en la instancia")
nombre_static.novedades = ("Novedades del día", "en la instancia")
print('* NombreStatic.NombresValidos', NombreStatic.NombresValidos())
print('* nombre_static.NombresValidos', nombre_static.NombresValidos())

print('* NombreStatic.novedades', NombreStatic.novedades)
print('* nombre_static.novedades', nombre_static.novedades)

print("Novedades en la clase")
NombreStatic.novedades = ("Nada nuevo", "en la clase")
print('* NombreStatic.NombresValidos', NombreStatic.NombresValidos())
print('* nombre_static.NombresValidos', nombre_static.NombresValidos())

print('* NombreStatic.novedades', NombreStatic.novedades)
print('* nombre_static.novedades', nombre_static.novedades)


# class Modelo:
#
#     def __init__(self, name):
#         self._name = name
#
#         @property
#         def name(self):
#             return self._name
#
#         @name.setter
#         def name(self, name):
#             if name not in LISTA_DE_NOMBRES:
#                 raise ValueError('%s no habilitado por la LISTA_DE_NOMBRES' % name)
#             self._name = name
#
#         # @@property
#         # def name(self):
#         #     return self._name
#         #
#         # @name.setter
#         # def name(s)
