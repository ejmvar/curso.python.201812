<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Python&#xa;(versiones)" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1553808782045"><hook NAME="MapStyle" zoom="1.877">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="9" RULE="ON_BRANCH_CREATION"/>
<node TEXT="2.x (2.7)" FOLDED="true" POSITION="right" ID="ID_1874151200" CREATED="1545056300054" MODIFIED="1553808812617" LINK="https://docs.python.org/2.7/">
<edge COLOR="#808080"/>
<node TEXT="2.4 (old stable)" ID="ID_632175322" CREATED="1546013652947" MODIFIED="1546013718127"/>
<node TEXT="2.6 (some 3.x backported)" ID="ID_1751655806" CREATED="1546013659728" MODIFIED="1546013735146"/>
<node TEXT="2.7 (now)" ID="ID_1933871563" CREATED="1546013677537" MODIFIED="1546013686667"/>
</node>
<node TEXT="3.x (3.7)" FOLDED="true" POSITION="right" ID="ID_251269685" CREATED="1545056330039" MODIFIED="1553808815352" LINK="https://docs.python.org/3/">
<edge COLOR="#808080"/>
<node TEXT="3.6" OBJECT="java.lang.Double|3.6" ID="ID_691292909" CREATED="1545061936137" MODIFIED="1551367502014"/>
<node TEXT="3.7.2" ID="ID_1000760299" CREATED="1551367503525" MODIFIED="1551367519404"/>
</node>
<node TEXT="Elecci&#xf3;n" FOLDED="true" POSITION="right" ID="ID_154744778" CREATED="1545056850313" MODIFIED="1553808818081" LINK="https://docs.python-guide.org/starting/which-python/">
<edge COLOR="#808080"/>
<node TEXT="x Soft existente" ID="ID_1993606600" CREATED="1545061825365" MODIFIED="1545061903918"/>
<node TEXT="x Caracter&#xed;sticas" ID="ID_1203222297" CREATED="1545061883729" MODIFIED="1545061896906"/>
<node TEXT="x Preferencias" ID="ID_208202458" CREATED="1545061861845" MODIFIED="1545061908032"/>
<node TEXT="historia" ID="ID_224656235" CREATED="1551367662100" MODIFIED="1551367688899" LINK="https://linuxacademy.com/blog/linux/the-story-of-python-2-and-3/"/>
<node TEXT="seguridad" ID="ID_706204556" CREATED="1551367666814" MODIFIED="1551367675832" LINK="https://snyk.io/blog/python-2-vs-3-security-differences"/>
</node>
<node TEXT="Comparativa 2 vs 3" POSITION="right" ID="ID_921360846" CREATED="1545315959199" MODIFIED="1553808820185" LINK="Comparativa_Syntax.mm">
<edge COLOR="#808080"/>
</node>
</node>
</map>
