<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Concurrencia" FOLDED="false" ID="ID_191153586" CREATED="1547483918471" MODIFIED="1547484996989" ICON_SIZE="36.0 pt" LINK="menuitem:_ExternalImageAddAction" STYLE="oval">
<font SIZE="22"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="15" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      https://docs.python.org/2/library/multiprocessing.html
    </p>
    <p>
      
    </p>
    <p>
      https://docs.python.org/3.4/library/multiprocessing.html?highlight=process
    </p>
    <p>
      https://docs.python.org/3.4/library/threading.html#module-threading
    </p>
    <p>
      
    </p>
    <p>
      https://realpython.com/python-concurrency/
    </p>
    <p>
      https://github.com/pedrolarben/Concurrecia_python/wiki/0.-Control-de-la-concurrencia-en-python
    </p>
    <p>
      
    </p>
    <p http-equiv="content-type" content="text/html; charset=utf-8" class="h3 mb-2 text-muted" style="margin-top: 0px; margin-bottom: 0; font-family: source sans pro, -apple-system, BlinkMacSystemFont, segoe ui, Roboto, helvetica neue, Arial, sans-serif, apple color emoji, segoe ui emoji, segoe ui symbol; font-weight: 500; line-height: 1.2; color: rgb(153, 153, 153) !important; font-size: 1.72266rem; font-style: normal; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px">
      Table of Contents
    </p>
    <div class="toc" style="color: rgb(34, 34, 34); font-family: source sans pro, -apple-system, BlinkMacSystemFont, segoe ui, Roboto, helvetica neue, Arial, sans-serif, apple color emoji, segoe ui emoji, segoe ui symbol; font-size: 18px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px">
      <ul style="margin-top: 0px; margin-bottom: 0px">
        <li style="margin-bottom: 0px; margin-top: 0px">
          <a href="https://realpython.com/python-concurrency/#what-is-concurrency" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">Concurrency?</font></a>
        </li>
        <li style="margin-bottom: 0px; margin-top: 0px">
          <a href="https://realpython.com/python-concurrency/#what-is-parallelism" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">Parallelism?</font></a>
        </li>
        <li style="margin-bottom: 0px; margin-top: 0px">
          <a href="https://realpython.com/python-concurrency/#when-is-concurrency-useful" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">Concurrency Useful?</font></a>
        </li>
        <li style="margin-bottom: 0px; margin-top: 0px">
          <a href="https://realpython.com/python-concurrency/#how-to-speed-up-an-io-bound-program" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">Speed Up : I/O-Bound Program</font></a>

          <ul style="margin-top: 0px; margin-bottom: 0px">
            <li style="margin-bottom: 0px; margin-top: 0px">
              <a href="https://realpython.com/python-concurrency/#synchronous-version" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">Synchronous</font></a>
            </li>
            <li style="margin-bottom: 0px; margin-top: 0px">
              <a href="https://realpython.com/python-concurrency/#threading-version" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">threading</font></a>
            </li>
            <li style="margin-bottom: 0px; margin-top: 0px">
              <a href="https://realpython.com/python-concurrency/#asyncio-version" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">asyncio</font></a>
            </li>
            <li style="margin-bottom: 0px; margin-top: 0px">
              <a href="https://realpython.com/python-concurrency/#multiprocessing-version" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">multiprocessing</font></a>
            </li>
          </ul>
        </li>
        <li style="margin-bottom: 0px; margin-top: 0px">
          <a href="https://realpython.com/python-concurrency/#how-to-speed-up-a-cpu-bound-program" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">Speed Up : CPU-Bound</font></a>

          <ul style="margin-top: 0px; margin-bottom: 0px">
            <li style="margin-bottom: 0px; margin-top: 0px">
              <a href="https://realpython.com/python-concurrency/#cpu-bound-synchronous-version" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">CPU-Bound Synchronous</font></a>
            </li>
            <li style="margin-bottom: 0px; margin-top: 0px">
              <a href="https://realpython.com/python-concurrency/#threading-and-asyncio-versions" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">threading and asyncio</font></a>
            </li>
            <li style="margin-bottom: 0px; margin-top: 0px">
              <a href="https://realpython.com/python-concurrency/#cpu-bound-multiprocessing-version" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">CPU-Bound multiprocessing</font></a>
            </li>
          </ul>
        </li>
        <li style="margin-bottom: 0px; margin-top: 0px">
          <a href="https://realpython.com/python-concurrency/#when-to-use-concurrency" style="color: rgb(54, 118, 171); text-decoration: none"><font color="rgb(54, 118, 171)">When to Use Concurrency</font></a>
        </li>
      </ul>
    </div>
  </body>
</html>

</richcontent>
<node TEXT="Procesos&#xa;y threads" POSITION="right" ID="ID_273436263" CREATED="1547483918484" MODIFIED="1547484549552" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<edge COLOR="#007c7c"/>
<node TEXT="IDEA 1.1" ID="ID_588194298" CREATED="1547483918484" MODIFIED="1547483918484" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-12.292683498693489 pt"/>
<node TEXT="IDEA 1.2" ID="ID_1104103844" CREATED="1547483918485" MODIFIED="1547483918485"/>
</node>
<node TEXT="GIL" POSITION="right" ID="ID_495676614" CREATED="1547483918485" MODIFIED="1547484639138" HGAP_QUANTITY="101.80488213352491 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<edge COLOR="#009900"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span http-equiv="content-type" content="text/html; charset=utf-8" style="color: rgb(36, 41, 46); font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 16px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="rgb(36, 41, 46)" face="-apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol" size="16px">GIL (Global Interpreter Lock) </font></span>
    </p>
    <p>
      
    </p>
    <p>
      <span http-equiv="content-type" content="text/html; charset=utf-8" style="color: rgb(36, 41, 46); font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 16px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="rgb(36, 41, 46)" face="-apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol" size="16px">Asigna tiempo fijo por hilo </font></span>
    </p>
    <p>
      <span http-equiv="content-type" content="text/html; charset=utf-8" style="color: rgb(36, 41, 46); font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 16px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="rgb(36, 41, 46)" face="-apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol" size="16px">Bloquea uno y desbloquea otro </font></span>
    </p>
    <p>
      
    </p>
  </body>
</html>

</richcontent>
<node TEXT="IDEA 2.1" ID="ID_744391648" CREATED="1547483918485" MODIFIED="1547483918485" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-14.926829962699236 pt"/>
<node TEXT="IDEA 2.2" ID="ID_903669612" CREATED="1547483918485" MODIFIED="1547483918485"/>
</node>
<node TEXT="IDEA 3" POSITION="right" ID="ID_90823870" CREATED="1547483918485" MODIFIED="1547483918485" HGAP_QUANTITY="56.146343424091945 pt" VSHIFT_QUANTITY="-61.46341749346744 pt">
<edge COLOR="#ff0000"/>
<node TEXT="IDEA 3.1" ID="ID_1201198400" CREATED="1547483918486" MODIFIED="1547483918486" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-15.804878784034486 pt"/>
<node TEXT="IDEA 3.2" ID="ID_1615683347" CREATED="1547483918486" MODIFIED="1547483918486" HGAP_QUANTITY="17.512195285340997 pt" VSHIFT_QUANTITY="3.512195285340997 pt"/>
</node>
<node TEXT="IDEA 6" POSITION="left" ID="ID_25434398" CREATED="1547483918486" MODIFIED="1547483918486" HGAP_QUANTITY="73.70731985079694 pt" VSHIFT_QUANTITY="-37.75609931741573 pt">
<edge COLOR="#cc00ff"/>
<node TEXT="IDEA 6.1" ID="ID_352937383" CREATED="1547483918486" MODIFIED="1547483918486" HGAP_QUANTITY="13.121951178664752 pt" VSHIFT_QUANTITY="-9.658537034687741 pt"/>
<node TEXT="IDEA 6.2" ID="ID_966834988" CREATED="1547483918486" MODIFIED="1547483918486" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-7.902439392017243 pt"/>
</node>
<node TEXT="IDEA 5" POSITION="left" ID="ID_1822402846" CREATED="1547483918487" MODIFIED="1547483918487" HGAP_QUANTITY="84.24390570681993 pt" VSHIFT_QUANTITY="-36.00000167474521 pt">
<edge COLOR="#ff9900"/>
<node TEXT="IDEA 5.1" ID="ID_1274507163" CREATED="1547483918487" MODIFIED="1547483918487" HGAP_QUANTITY="14.0 pt" VSHIFT_QUANTITY="-21.07317171204598 pt"/>
<node TEXT="IDEA 5.2" ID="ID_1203076641" CREATED="1547483918487" MODIFIED="1547483918487"/>
</node>
<node TEXT="IDEA 4" POSITION="left" ID="ID_575079876" CREATED="1547483918487" MODIFIED="1547483918487" HGAP_QUANTITY="67.56097810145019 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<edge COLOR="#0000ff"/>
<node TEXT="IDEA 4.1" ID="ID_317662625" CREATED="1547483918487" MODIFIED="1547483918487" HGAP_QUANTITY="14.878048821335248 pt" VSHIFT_QUANTITY="-18.439025248040235 pt"/>
<node TEXT="IDEA 4.2" ID="ID_1522387997" CREATED="1547483918488" MODIFIED="1547483918488"/>
</node>
</node>
</map>
