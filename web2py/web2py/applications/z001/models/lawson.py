# -*- coding: utf-8 -*-

"""
Modelos:
- Usuarios (auth_user)
- Roles de usuarios (auth_group)
- Relación Roles-Usuarios (auth_membership)
- Proyectos
- Relación Proyectos-Usuarios (ignora rango fechas)
- Relación Proyectos-Lideres (asume un líder autorizador por proyecto)
- Asignaciones: (Usuario, Proyecto, Horas)


- Informe faltantes (Fecha, Usuario, Carga Existente - Semanal minima)


» db.auth_user
» db.auth_group
» db.auth_membership
» db.auth_permission
» db.auth_event
» db.auth_ca



"""

# -*- coding: utf-8 -*-
#################################################################################
# # #from gluon.validators import   IS_HTTP_URL
# # from gluon.validators import  Validator.IS_URL #  IS_HTTP_URL
# from gluon.dal import _default_validators.IS_URL as IS_HTTP_URL

if 0: # Models / Controllers # 20131001-1029
    from gluon import * ; from gluon.tools import Auth, Crud, Mail, Service, PluginManager
    from gluon.validators import * ; from gluon.sqlhtml import SQLFORM, SQLTABLE, form_factory
    from gluon.sql import *  ; from gluon.storage import Storage ; from gluon.html import *
    from gluon.http import * ; from gluon.globals import * ; from gluon.cache import Cache
    from gluon.languages import translator
    session, request, response = Session(), Request(), Response()
    cache , T = Cache(request) , translator(request)
    db , sgcc_settings = DAL() , Storage()
    auth, crud, mail, service, plugins = Auth(db), Crud(db), Mail(), Service(), PluginManager()
    logger = logging.getLogger("w2p.app.sgcc")
    logger.setLevel(logging.DEBUG)
#################################################################################
import logging
logger = logging.getLogger("web2py.app.myapp")
logger.setLevel(logging.DEBUG)

# db =DAL('sqlite://lawson.db')

# - Usuarios (auth_user)
# - Roles de usuarios (auth_group)
# - Relación Roles-Usuarios (auth_membership)
# - Proyectos

db.define_table("proyectos",

                Field("proy_code",),
                Field("proy_name",),
                Field("proy_descr",),
                Field("user_id", db.auth_user, ), # 'reference auth_user'),
                Field("leader_id", db.auth_user, ), # 'reference auth_user'),

                Field("activo", 'boolean'),
                Field("activo_desde", 'date'),
                Field("activo_hasta", 'date'),

                format='%(proy_code)s - %(proy_name)s',
                )

# - Relación Proyectos-Usuarios (ignora rango fechas)

db.define_table("lideres",


                Field("proy_code", ),
                Field("proy_name",),
                Field("proy_descr",),
                Field("user_id", db.auth_user), #'reference auth_user'),
                Field("leader_id", db.auth_user), #'reference auth_user'),

                Field("activo", 'boolean'),
                Field("activo_desde", 'date'),
                Field("activo_hasta", 'date'),

                format='%(proy_code)s - %(proy_name)s',
                )
db.lideres.proy_code.requires=IS_IN_DB(db, db.proyectos.proy_code)

# # REDUNDANTES:
# db.proyectos.leader_id.requires=IS_IN_DB(db, db.lideres.id)

# - Relación Proyectos-Lideres (asume un líder autorizador por proyecto)


# # - Asignaciones: (Usuario, Proyecto, Horas)
# db.define_table("asignaciones",
#
#                 Field("rol", auth_group, ),  # reference )
#                 Field("rol", auth_group, ),  # reference )
#
#                 format="",
#                 )

# - Tipo Tarea
db.define_table("tipo_tareas",

                Field("codigo"),
                Field("descripc"),

                Field("proy_code", ),

                )
# - Detalle Tareas
db.define_table("tareas",

                Field("fecha", 'date'),
                Field("horas", 'integer'),

                Field("proyecto", db.proyectos),
                Field("proy_code", ),

                Field("codigo"),
                Field("codigo"),
                Field("descripc"),


                )
db.tareas.proy_code.requires=IS_IN_DB(db, db.proyectos.proy_code)


