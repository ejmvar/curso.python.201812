#!/usr/bin/env python3
# *-* coding: cp1252 *-*
# *-* coding: utf-8 *-*

# Ejemplo:
# * recorrer archivo "CIUDADES DEL MUNDO"
# * tomar columna POBLACION
# * filtrar NROS vs valores mal formateadas
# * entregar acumulado POBLACION MUNDIAL

from __future__ import print_function
import time
import six
import sys

print(sys.stdout.encoding)

# !!! Corregir PATH y nombre del archivo utilizado !!!
map_file = "../mapinfo/geonames.org/1000_allCountries.txt"  # 1000 ciudades
map_file = "./1000_allCountries.txt"  # 1000 ciudades
# map_file = "../mapinfo/geonames.org/allCountries.txt"       # 11.896.963 ciudades

COL_POBLACION = 14  # columna de interés, offset 0 (=> col 15)


def lapso(t0):
    "Delta time en segs o msegs"
    dt_secs = time.time() - t0

    return ("Lapso: %s%s " % (
                    (round(dt_secs, 3), "s")            if dt_secs >= 1
            else    (round(dt_secs*1000, 3), "ms")
        )
    )


def calcular_poblacion():
    """Acumula población de las ciudades que figuran en el archivo

        Implementa Generadores/Iteradores en "tubería"
        Lee/Filtra/Calcula al momento de entregar el total

        Imprime el lapso desde el inicio a cada paso
            para mostrar dónde se realiza la tarea

    """
    t00 = time.time()


    # with open(map_file) as file:
    # with open(map_file, 'r+', encoding="utf-8") as file:
    if six.PY2:
        fh = open(map_file, 'r+')
    else:
        fh = open(map_file, 'r+', encoding="utf-8")

    with fh as file:

        print("File handler: %s\n\n" % file)
        # print("Paso 0: ", time.time() - t00)
        print("Paso 0: ", lapso(t00))

        # GEN con el vector de datos (field separator = tab )
        data_col = (line.split('\t')[COL_POBLACION] for line in file)
        print("Paso 1: ", lapso(t00))

        # GEN filtro de datos inválidos
        poblacion = (int(x) for x in data_col if x != 'N/A')
        print("Paso 2: ", lapso(t00))

        # EJECUCION **REAL**
        # print("1 ErrPy3 Población total = ", sum(poblacion))
        # print("2 ErrPy2 Población total = ".encode('latin-1') , sum(poblacion))
        # print("3 ErrPy2 Población total = ".encode('utf-8') , sum(poblacion))
        print("3 Acumulado = ".encode('utf-8') , sum(poblacion))

    #t01 = time.time() - t00
    # print("Paso 2: ", lapso(t00))
    print("*Final: ", lapso(t00))

    print("Archivo cerrado por WITH?:", file.closed)
    print("="*50)

    print("Registros: 11.896.963")

    # Tiempo incurrido... (en segs o msegs, según convenga)
    print("*Sale*: ", lapso(t00))
    # print("Lapso: {}{} ".format(
    #     round(t01, 3) if t01 >= 1 else round(t01*1000, 3),
    #     "s" if t01 >= 1 else "ms",
    # )
    # )


# if __name__=="if __name__ == "__main__":
if __name__ == "__main__":

    calcular_poblacion()
