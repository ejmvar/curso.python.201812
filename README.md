Webinar: introducción / capacitación en Python


* Repositorios:
    * Este contenido está hosteado en:
      * https://gitlab.com/ejmvar/Curso.Python.201812
      * https://github.com/ejmvar/Curso.Python.201812
      * Wiki: https://gitlab.com/ejmvar/curso.python.201812/wikis/home
      
        * [Para comenzar:](https://gitlab.com/ejmvar/curso.python.201812/wikis/Para-comenzar)
  

* Herramientas utilizadas y setup:
    Sugeridas (open source, y algunas alternativas, propietarias o no):

    Leer SETUP.md
    
Estructura del webinar:

