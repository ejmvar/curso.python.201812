# PythonDecorators/entry_exit_class.py
class entry_exit(object):

    def __init__(self, f):
        print("@ init")
        self.f = f

    def __call__(self):
        print("@ Entering", self.f.__name__)
        self.f()
        print("@ Exited", self.f.__name__)

@entry_exit
def func1():
    print("inside func1()")

@entry_exit
def func2():
    print("inside func2()")

if __name__=="__main__":
    print("* inicio")

    func1()
    func2()

    print("* final")
