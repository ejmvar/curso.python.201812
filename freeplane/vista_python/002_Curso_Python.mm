<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Python avanzado" FOLDED="false" ID="ID_191153586" CREATED="1547495961950" MODIFIED="1553785073152" ICON_SIZE="36.0 pt" LINK="http://localhost:8888/tree/PYTHON_avanzado" STYLE="oval">
<font SIZE="22"/>
<hook NAME="MapStyle" zoom="0.909">
    <properties fit_to_viewport="false" show_note_icons="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" backgroundImageURI="../../logo/python-logo-generic.svg"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="37" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<hook URI="python-logo-generic-11-2.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Herramientas" POSITION="right" ID="ID_273436263" CREATED="1547495961959" MODIFIED="1551465046938" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<edge COLOR="#007c7c"/>
<node TEXT="Int&#xe9;rprete" ID="ID_1739129483" CREATED="1551539904812" MODIFIED="1551539914353">
<node TEXT="python2" ID="ID_1121912883" CREATED="1551539915890" MODIFIED="1551539930083">
<node TEXT="idle" ID="ID_1888810102" CREATED="1551540052083" MODIFIED="1551540055111"/>
</node>
<node TEXT="python3" ID="ID_738763995" CREATED="1551539931090" MODIFIED="1551539935768">
<node TEXT="ipython" ID="ID_1729867477" CREATED="1551539937043" MODIFIED="1551539946504"/>
</node>
</node>
<node TEXT="IDEs" ID="ID_613301783" CREATED="1547495961959" MODIFIED="1551465157084" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<node TEXT="MS VSCode" ID="ID_833699200" CREATED="1551457067539" MODIFIED="1551457073165"/>
<node TEXT="Pycharm" ID="ID_1637908174" CREATED="1551457074870" MODIFIED="1551457080146"/>
</node>
<node TEXT="notebook" ID="ID_348011905" CREATED="1551457019628" MODIFIED="1551888604930" LINK="https://jupyter.readthedocs.io/en/latest/install.html">
<node TEXT="ipython" ID="ID_604228650" CREATED="1551457085552" MODIFIED="1551457096327"/>
<node TEXT="jupyter (anaconda)" ID="ID_1643422030" CREATED="1551457099728" MODIFIED="1551888668547" LINK="http://localhost:8888/tree"/>
</node>
</node>
<node TEXT="Tips iniciales" POSITION="right" ID="ID_634869024" CREATED="1551888698280" MODIFIED="1551888837109" LINK="http://localhost:8888/tree/PYTHON_basico">
<edge COLOR="#00ffff"/>
<node TEXT="print Py2/Py3" ID="ID_1379999045" CREATED="1547495961959" MODIFIED="1551540687584" LINK="http://localhost:8888/notebooks/compatibilizar_Py2_future.ipynb" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<node TEXT="PY2 print es sentencia" ID="ID_1951009310" CREATED="1551889166052" MODIFIED="1551889214162" LINK="http://localhost:8888/notebooks/PYTHON_basico/000%20PY2%20print.ipynb"/>
<node TEXT="PY3: print() esfunci&#xf3;n" ID="ID_1490224175" CREATED="1551889190453" MODIFIED="1551889207524" LINK="http://localhost:8888/notebooks/PYTHON_basico/000%20PY3%20print.ipynb"/>
<node TEXT="from future" ID="ID_1051570213" CREATED="1551889269283" MODIFIED="1551889287775" LINK="http://localhost:8888/notebooks/PYTHON_basico/000%20PY2%20-%20PY3%20from%20future.ipynb"/>
<node TEXT="diferencia entre sentencia y funci&#xf3;n" ID="ID_1579483140" CREATED="1551889305621" MODIFIED="1551889321661" LINK="http://localhost:8888/notebooks/compatibilizar_Py2_future.ipynb"/>
<node TEXT="SIX" ID="ID_1871038738" CREATED="1551889396220" MODIFIED="1551889409945" LINK="http://localhost:8888/notebooks/compatibilizar_Six_Py2_Py3.ipynb"/>
</node>
<node TEXT="Ayuda en l&#xed;nea" ID="ID_1441481189" CREATED="1551890935159" MODIFIED="1551890942017">
<node TEXT="help(__future__)" ID="ID_846969476" CREATED="1551890943238" MODIFIED="1551890950878"/>
</node>
<node TEXT="introspecci&#xf3;n" ID="ID_1424196428" CREATED="1551889329776" MODIFIED="1551889336763">
<node TEXT="dir()" ID="ID_877076592" CREATED="1551889343981" MODIFIED="1551889348471"/>
<node TEXT="id()" ID="ID_221975349" CREATED="1551896530138" MODIFIED="1551896533561"/>
</node>
<node TEXT="Indentado" ID="ID_1552588969" CREATED="1551892123365" MODIFIED="1551892549689" LINK="http://localhost:8888/notebooks/PYTHON_basico/001%20Indentado.ipynb"/>
<node TEXT="Codificaci&#xf3;n" ID="ID_1430965245" CREATED="1551892698230" MODIFIED="1551892703966">
<node TEXT="en general" ID="ID_1223204225" CREATED="1551892803708" MODIFIED="1551892874442" LINK="file:/home/Neoris/curso.python.201812.git/curso.python.201812/Curso.py3/000-000-presentacion.py">
<node TEXT="V1" ID="ID_1276970462" CREATED="1551892926258" MODIFIED="1551892950427" LINK="http://localhost:8888/notebooks/PYTHON_basico/002%20Codificaci%C3%B3n.ipynb"/>
</node>
<node TEXT="en scripts&apos;" ID="ID_208781991" CREATED="1551892812898" MODIFIED="1551892821819"/>
</node>
</node>
<node TEXT="Objetos" POSITION="right" ID="ID_919167013" CREATED="1551463889841" MODIFIED="1551463904319">
<edge COLOR="#ff00ff"/>
<node TEXT="Built-ins" FOLDED="true" ID="ID_550774253" CREATED="1551463906912" MODIFIED="1553785051871" LINK="https://docs.python.org/3/library/stdtypes.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      numerics, sequences, mappings, classes, instances, exceptions
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Num&#xe9;ricos" ID="ID_1670788824" CREATED="1551888440390" MODIFIED="1551888451036">
<node TEXT="PY2: Enteros y Long" ID="ID_1707054989" CREATED="1551888455962" MODIFIED="1551896977688" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Numericos-PY2.ipynb"/>
<node TEXT="PY3: siempre Enteros" ID="ID_1127426454" CREATED="1551896344569" MODIFIED="1551897122528" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Numericos-PY3.ipynb"/>
<node TEXT="Float, Complex" ID="ID_1502062634" CREATED="1551888485094" MODIFIED="1551898086569"/>
<node TEXT="Fractions" ID="ID_1995637959" CREATED="1551898740996" MODIFIED="1551898774412" LINK="https://docs.python.org/3.1/library/fractions.html"/>
</node>
<node TEXT="Secuencias" ID="ID_1328541668" CREATED="1551891225225" MODIFIED="1551891228754">
<node TEXT="PY2 listas vs iterables" ID="ID_1307682106" CREATED="1551900000980" MODIFIED="1551900017408"/>
<node TEXT="PY3 iterables" ID="ID_191926738" CREATED="1551900018612" MODIFIED="1551900024459"/>
</node>
<node TEXT="Sets / Dicts" ID="ID_1700846686" CREATED="1551464140354" MODIFIED="1551897214827" LINK="http://localhost:8888/notebooks/PYTHON_basico/003-Objetos-Set-Dict.ipynb"/>
<node TEXT="Excepciones" ID="ID_1482379177" CREATED="1551891229848" MODIFIED="1551891255241"/>
</node>
<node TEXT="print y representaci&#xf3;n" ID="ID_1882711244" CREATED="1551464553965" MODIFIED="1551464569120">
<node TEXT="concatenando" ID="ID_1610398594" CREATED="1551891358769" MODIFIED="1551891370466"/>
<node TEXT="format tipo C" ID="ID_1997193408" CREATED="1551891371902" MODIFIED="1551891392116"/>
<node TEXT="format &quot;moderno&quot;" ID="ID_336542986" CREATED="1551891392474" MODIFIED="1551891405204"/>
</node>
<node TEXT="tests V/F" ID="ID_1388475411" CREATED="1551464579761" MODIFIED="1551901206468"/>
<node TEXT="Idioms" ID="ID_1930758380" CREATED="1551463923406" MODIFIED="1551463927324"/>
</node>
<node TEXT="Organizaci&#xf3;n y orientaci&#xf3;n" POSITION="left" ID="ID_1119755622" CREATED="1547498496749" MODIFIED="1552999457188" LINK="Organizacion.mm">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="Sentencias compuestas" POSITION="left" ID="ID_1522692698" CREATED="1553784362901" MODIFIED="1553784585970">
<edge COLOR="#ff00ff"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <a href="https://docs.python.org/2/reference/compound_stmts.html"><b><font size="3">PY2 </font></b></a>
    </p>
    <p>
      <a href="https://docs.python.org/3/reference/compound_stmts.html"><b><font size="3">PY3 </font></b></a>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="if:&#xa;elif:&#xa;else:" POSITION="left" ID="ID_484645938" CREATED="1552997634347" MODIFIED="1552997713546">
<edge COLOR="#7c7c00"/>
</node>
<node TEXT="iterators" POSITION="left" ID="ID_1875108181" CREATED="1552998358904" MODIFIED="1552998365241">
<edge COLOR="#00ff00"/>
<node TEXT="itertools" ID="ID_1885934516" CREATED="1552999091171" MODIFIED="1552999097930" LINK="https://docs.python.org/3.4/library/itertools.html"/>
</node>
<node TEXT="for:&#xa;continue/break&#xa;else:" POSITION="left" ID="ID_335539509" CREATED="1552997647440" MODIFIED="1552997706262">
<edge COLOR="#ff0000"/>
<node TEXT="iterator&#xa;protocol" ID="ID_712326255" CREATED="1552998118794" MODIFIED="1552998132172">
<node TEXT="next()" ID="ID_1096582282" CREATED="1552998133618" MODIFIED="1552998140461"/>
<node TEXT="StopIteration" ID="ID_189210969" CREATED="1552998596599" MODIFIED="1552998605094"/>
</node>
</node>
<node TEXT="with&#xa;continue/break&#xa;else:" POSITION="left" ID="ID_507660974" CREATED="1552997487084" MODIFIED="1552997721632">
<edge COLOR="#00007c"/>
<node TEXT="context&#xa;protocol" ID="ID_1431757782" CREATED="1552997514827" MODIFIED="1552998077896">
<node TEXT="__enter__()" ID="ID_905020827" CREATED="1552997495715" MODIFIED="1552997502656"/>
<node TEXT="__exit()" ID="ID_1137947492" CREATED="1552997504418" MODIFIED="1552997508877"/>
</node>
</node>
<node TEXT="try:&#xa;catch Exception as exc&#xa;finally:&#xa;else:" POSITION="left" ID="ID_874470277" CREATED="1552997668470" MODIFIED="1552997699108">
<edge COLOR="#0000ff"/>
</node>
<node POSITION="left" ID="ID_626565175" CREATED="1551965629946" MODIFIED="1553799587607" LINK="http://localhost:8888/notebooks/PYTHON_avanzado/Decoradores.ipynb"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Decorators
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <a href="https://python-3-patterns-idioms-test.readthedocs.io/en/latest/PythonDecorators.html"><font size="3">python-3-patterns-idioms</font></a>
    </p>
  </body>
</html>
</richcontent>
<edge COLOR="#7c0000"/>
</node>
</node>
</map>
